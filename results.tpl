\subsubsection*{Time stepping method ``%(STEPPING_METHOD)s''}
  \begin{equation}
    \mathcal{L}_1 = %(L1_EXPRESSION)s
  \end{equation}
  which gives
  \begin{equation}
   \begin{aligned}
    v^{(P)}_1 &= %(V_P)s \\
    v^{(Q)}_1 &= %(V_Q)s \\
    \Sigma^{(QP)} &= %(SIGMA_QP)s \\
    \Sigma^{(PP)} &= %(SIGMA_PP)s
   \end{aligned}
  \end{equation}
