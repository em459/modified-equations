\documentclass{article}
\usepackage{amsmath,amssymb}
\usepackage[cm]{fullpage}
\begin{document}
\section*{Modified equations}
  Modified equations derived as in \cite{Zygalakis2011}.
  \subsection*{Definitions}
  The lowest order generator $\partial \phi/\partial t = \mathcal{L}_0[\phi]$ of the SDE
  \begin{equation}
   \begin{aligned}
    dP &= -(\lambda(Q) P - V'(Q))\;dt + \sigma(Q)\; dW(t) \\
    dQ &= P\;dt
   \end{aligned}
  \end{equation}
  is given by
  \begin{equation}
    \mathcal{L}_0[\phi] = -(\lambda(Q) P+V'(Q))\frac{\partial\phi}{\partial P} + P\frac{\partial\phi}{\partial Q} + \frac{\sigma(Q)^2}{2}\frac{\partial^2 \phi}{\partial P^2}
  \end{equation}
  Then the generator of the modified equations is
  \begin{equation}
   \begin{aligned}
    \mathcal{L} &= \mathcal{L}_0 + h \mathcal{L}_1 + \mathcal{O}(h^2)\\
    \mathcal{L}_1 &= \mathcal{A}_1 - \frac{1}{2}\mathcal{L}_0^2
   \end{aligned}
  \end{equation}
  where (see equations (4.7) and (4.8) in \cite{Zygalakis2011})
  \begin{equation}
    \mathcal{L}_1[\phi] = v^{(P)}_1 \frac{\partial \phi}{\partial P} + v^{(Q)}_1 \frac{\partial \phi}{\partial Q} + \sigma \Sigma^{(QP)}_1 \frac{\partial^2 \phi}{\partial P\partial Q}
+ \sigma \Sigma^{(PP)}_1 \frac{\partial^2 \phi}{\partial P^2}
  \end{equation}
  which results in the modified equation
  \begin{equation}
    \begin{aligned}
    dP &= \left[-(\lambda(Q) P - V'(Q))+h v^{(P)}_1(P,Q)\right]\;dt + (\sigma(Q)+h\Sigma^{(PP)}_1(P,Q))\; dW(t) \\
    dQ &= \left[P+h v^{(Q)}_1(P,Q)\right]\;dt + h\Sigma^{(QP)}_1(P,Q)\;dW(t)
   \end{aligned}
  \end{equation}
  \subsection*{Results}
  %(RESULTS)s
  \bibliographystyle{unsrt}
  \bibliography{notes}
\end{document}
