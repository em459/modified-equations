from __future__ import print_function
import sys
from sympy import *

'''Calculate modified equations for SDE of Langevin type

    dP = -lambda*P*dt + sigma*dW(t)
    dQ = P*dt

'''

# **** Define symbols ****

# Momentum P
P = Symbol('P')
# Position Q
Q = Symbol('Q')
# Square root of time step size h and time step size h
# We need this sympy can't expand in fractional powers, and there will be terms of
# O(h^{1/2}) in the expansion, which only disappear upon integration over all states
sqrt_h = Symbol('sqrt_h')
h = Symbol('h')

# Set this flag to True if lambda and sigma are constant
constant_profiles = False

# Set this flag to true if the potential only depends on Q
spatial_only_potential = False

if (constant_profiles):
    # Parameter lambda
    lmbda_ = Symbol('lambda')
    lmbda = (lambda x : lmbda_)
    # Strength of random forcing
    sigma_ = Symbol('sigma')
    sigma = (lambda x : sigma_)
else:
    # Parameter lambda
    lmbda = Function('lambda')
    # Strength of random forcing
    sigma = Function('sigma')

if (spatial_only_potential):
    # Derivative of function in Langevin equation
    dV_ = Function('dV')
    dV = (lambda x,y: dV_(y))
else:
    # Derivative of function in Langevin equation
    dV = Function('dV')

# Normally distributed random number xi
xi = Symbol('xi')
xi_P = Symbol('xi_P',real=True)
xi_Q = Symbol('xi_Q',real=True)
# Test function
phi = Function('phi')

# **** Define required subroutines ****

class Timestep(object):
    '''Timestep base class'''

    def __init__(self):
        self.label = ''

    def L0(self,psi):
        '''Lowest order generator of SDE
            dpsi/dt = L*psi, L = L_0 + h*L_1 + O(h^2)
        '''
        return -(lmbda(Q)*P+dV(P,Q))*diff(psi,P) + P*diff(psi,Q)+sigma(Q)**2/2*diff(psi,P,2)
    def A1(self,psi,P_dt,Q_dt):
        '''Calculate A_1 for a specific time stepping method given by
            P_dt and Q_dt
        '''
        expr = psi(P_dt,Q_dt)
        # Expansion in h
        h_coeff = expr.series(sqrt_h,0,5).removeO().coeff(sqrt_h**4)
        integ_P = integrate(1/sqrt(2*pi)*exp(-xi_P**2/2)*h_coeff,(xi_P,-oo,oo))
        integ_Q = integrate(1/sqrt(2*pi)*exp(-xi_Q**2/2)*integ_P,(xi_Q,-oo,oo))
        return integ_Q

    def L1(self):
        '''Calculate the 2nd order term in the modified generator.'''
        P_dt, Q_dt = self.dPdQ()
        return expand(simplify(self.A1(phi,P_dt,Q_dt) - self.L0(self.L0(phi(P,Q)))/2))

    def latex(self):
        # Extract the coefficients needed in the expansion
        L1 = self.L1()
        v_Q = L1.coeff(diff(phi(P,Q),Q))
        v_P = L1.coeff(diff(phi(P,Q),P))
        Sigma_QP = L1.coeff(diff(phi(P,Q),P,Q))/sigma(Q)
        Sigma_PP = L1.coeff(diff(phi(P,Q),P,2))/sigma(Q)
        result_dict = {'STEPPING_METHOD':self.label,
                       'L1_EXPRESSION':latex(L1),
                       'V_P':latex(v_P),
                       'V_Q':latex(v_Q),
                       'SIGMA_QP':latex(Sigma_QP),
                       'SIGMA_PP':latex(Sigma_PP)}
        # File with LaTeX template
        template_file = open('results.tpl','r')
        # Resulting latex file
        s = template_file.read() % result_dict
        template_file.close()
        return s

class TimestepGeometricLangevinEuler(Timestep):
    '''Class for the Geometric Langevin method'''
    def __init__(self):
        self.label = 'Geometric Langevin'

    def dPdQ(self):
        '''Return the time step P(dt), Q(dt) of the splitting method.
            Note that everything is expressed in term of powers of \sqrt{h}
        '''
        # Expand alpha first, to make life easier for the integrator
        alpha = (sqrt((1-exp(-2*lmbda(Q)*sqrt_h**2))/(2*lmbda(Q)))).series(sqrt_h,0,5).removeO()
        P_dt = exp(-lmbda(Q)*sqrt_h**2)*P+sigma(Q)*alpha*xi_P
        P_dt = (P_dt - dV(P_dt,Q)*sqrt_h**2).series(sqrt_h,0,5).removeO()
        Q_dt = Q + P_dt*sqrt_h**2
        return P_dt, Q_dt

class TimestepModifiedSplitting(Timestep):
    '''Class for the bivariate splitting method'''

    def __init__(self):
        self.label = 'Bivarirate Splitting'

    def dPdQ(self):
        '''Return the time step P(dt), Q(dt) of the modified splitting method.
            Note that everything is expressed in term of powers of \sqrt{h}
        '''
        # Expand alpha first, to make life easier for the integrator
        Covar_rP_rQ = sigma(Q)**2/(2*lmbda(Q)**2)*(1-2*exp(-lmbda(Q)*sqrt_h**2)+exp(-2*lmbda(Q)*sqrt_h**2))
        Var_rP = sigma(Q)**2/(2*lmbda(Q))*(1-exp(-2*lmbda(Q)*sqrt_h**2))
        Var_rQ = sigma(Q)**2/(2*lmbda(Q)**3)*(2*lmbda(Q)*sqrt_h**2-3+4*exp(-lmbda(Q)*sqrt_h**2)-exp(-2*lmbda(Q)*sqrt_h**2))
        L_PP = sqrt(Var_rP)
        L_QP = Covar_rP_rQ/sqrt(Var_rP)
        L_QQ = sqrt(Var_rQ-Covar_rP_rQ**2/Var_rP)
        a_PP=L_PP.series(sqrt_h,0,5).removeO()
        a_QQ=L_QQ.series(sqrt_h,0,5).removeO()
        a_QP=L_QP.series(sqrt_h,0,5).removeO()
        C1 = (1-exp(-lmbda(Q)*sqrt_h**2))/lmbda(Q)
        C2 = (lmbda(Q)*sqrt_h**2+exp(-lmbda(Q)*sqrt_h**2)-1)/lmbda(Q)**2
        P_dt = exp(-lmbda(Q)*sqrt_h**2)*P-C1*dV(P,Q)+a_PP*xi_P
        Q_dt = Q + (1-exp(-lmbda(Q)*sqrt_h**2))/lmbda(Q)*P -C2*dV(P,Q)+ a_QP*xi_P + a_QQ*xi_Q
        return P_dt, Q_dt

class TimestepOBASplitting(Timestep):
    '''class for generic splitting method based on the function O, B and A

    See: Leimkuhler, B., & Matthews, C. (2002).
    "Molecular dynamics: With Deterministic and Stochastic Numerical Methods".
    Interdisciplinary Applied Mathematics, 39.
    '''

    def __init__(self,label):
        '''Create method based on label, which can be, for example:
        "OBA", "BOAOB" etc.
        '''
        self.label = label
        self._factor = {}
        # Check that all characters are valid and calculate the factors
        for c in self.label:
            if (not (c in 'OAB')):
                print ('ERROR: invalid character in label : \"'+str(c)+'\"')
                sys.exit()
            try:
                self._factor[c] += 1
            except:
                self._factor[c] = 1
        self._func = {'A':self.A,
                      'B':self.B,
                      'O':self.O}

    def O(self,Xin,factor=1):
        alpha = (sqrt((1-exp(-2*lmbda(Xin[1])*sqrt_h**2/factor))/(2*lmbda(Xin[1])))).series(sqrt_h,0,5).removeO()
        return [exp(-lmbda(Xin[1])*sqrt_h**2/factor)*Xin[0]+sigma(Xin[1])*alpha*xi_P,Xin[1]]

    def A(self,Xin,factor=1):
        return  [Xin[0],Xin[1] + Xin[0]*sqrt_h**2/factor]

    def B(self,Xin,factor=1):
        dVexpanded = dV(Xin[0],Xin[1]).series(sqrt_h,0,3).removeO()
        return [Xin[0]-dVexpanded*sqrt_h**2/factor, Xin[1]]

    def dPdQ(self):
        '''Return the time step P(dt), Q(dt) 
            Note that everything is expressed in term of powers of \sqrt{h}
        '''
        X = (P,Q)
        for c in (self.label):
            X = self._func[c](X,self._factor[c])
        return X[0],X[1]    

results = ''
results += TimestepModifiedSplitting().latex()
for label in ('OBA','BAOAB','ABOBA','OBABO'):
    results += TimestepOBASplitting(label).latex()

# File with LaTeX template
template_file = open('notes.tpl','r')
# Resulting latex file
latex_file = open('notes.tex','w')
latex_file.write(template_file.read() % {'RESULTS':results})
template_file.close()
latex_file.close()
